<!DOCTYPE html>
<html lang="en">
<head>
    <title>subastadegigas.com</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--<script src="//cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="js/login.js"></script>
    <script src="js/login.response.handler.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<!--    <link href="//cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.css" type="text/css" rel="stylesheet" />-->
    <link rel="stylesheet" href="css/login.css">


</head>
<body>
    <div id="logreg-forms">
        <form class="form-signin">
            <h1 class="h3 mb-3 font-weight-normal" style="text-align: center"> Ingresar</h1>
            <input type="email" id="inputEmail" class="form-control" placeholder="Email" required="" autofocus="">
            <input type="password" id="inputPassword" class="form-control" placeholder="Contraseña" required="">
            
            <button class="btn btn-success btn-block" type="submit"><i class="fas fa-sign-in-alt"></i> Ingresar</button>
            <a href="#" id="forgot_pswd">Perdiste la contraseña?</a>
            <hr>
            <!-- <p>Don't have an account!</p>  -->
            <button class="btn btn-primary btn-block" type="button" id="btn-signup"><i class="fas fa-user-plus"></i> Crear nueva cuenta</button>
            </form>

            <form action="user_action.php" class="form-reset" method="post" id="passreset">
                <input type="hidden" name="action" value="lost_password">
                <input type="email" id="resetEmail" class="form-control" name="email" placeholder="Email " required="" autofocus="">
                <button class="btn btn-primary btn-block" type="submit">Reestablecer Contraseña</button>
                <a href="#" id="cancel_reset"><i class="fas fa-angle-left"></i> Atras</a>
            </form>
            
            <form action="user_action.php" class="form-signup" method="post" id="usercreated">
                <input type="hidden" name="action" value="signup">
                <input type="text" name="user-name" id="user-name" class="form-control" placeholder="Nombre COmpleto" required="" autofocus="">
                <input type="email" name="email" id="user-email" class="form-control" placeholder="Email" required autofocus="">
                <select name="operator" class="form-control">
                    <option value="Personal">Personal</option>
                    <option value="Personal">Movistar</option>
                    <option value="Personal">Claro</option>
                </select>+54 9 <input name="cell-phone" type="text" id="cell-phone" class="form-control-cell" placeholder="1123456789">
                <input type="password" name="user-pass" id="user-pass" class="form-control" placeholder="Contraseña" required autofocus="">
                <input type="password" name="user-repeatpass" id="user-repeatpass" class="form-control" placeholder="Repetir Contraseña" required autofocus="">

                <button class="btn btn-primary btn-block" type="submit"><i class="fas fa-user-plus"></i> Crear</button>
                <a href="#" id="cancel_signup"><i class="fas fa-angle-left"></i> Atras</a>
            </form>
            <br>
            
    </div>
</body>
</html>