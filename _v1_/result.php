<!DOCTYPE html>
<html lang="en">
    <head>
        <title>subastadegigas.com</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
    </head>
    <body>
        <div id="container">
            <div id="success-box">
                <div class="dot"></div>
                <div class="dot two"></div>
                <div class="face">
                    <div class="eye"></div>
                    <div class="eye right"></div>
                    <div class="mouth happy"></div>
                </div>
                <div class="shadow scale"></div>
                <div class="message"><h1 class="alert">Exito!</h1><p><?php echo $_POST['msj']; ?></p></div>
                <button class="button-box"><h1 class="green">Cerrar</h1></button>
            </div>
            <div id="error-box">
                <div class="dot"></div>
                <div class="dot two"></div>
                <div class="face2">
                    <div class="eye"></div>
                    <div class="eye right"></div>
                    <div class="mouth sad"></div>
                </div>
                <div class="shadow move"></div>
                <div class="message"><h1 class="alert">Error!</h1><p><?php echo $_POST['msj']; ?></p></div>
                <button class="button-box"><h1 class="red">Cerrar</h1></button>
            </div>
        </div>
    </body>
</html>